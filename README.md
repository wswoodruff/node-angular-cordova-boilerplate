# node-angular-cordova-boilerplate
=============================
> Uses code from https://github.com/mnill for hot update

**Attention**
This repo uses cached plugins and updates to those will not be in effect unless the plugins get reinstalled. This decision was made solely for speed of setup.

## Rename:

1. In /package.json rename the "name" property

2. In /server/cordova/www/config.xml, change these: <br>
**\<widget id="com.wsw.project"** <br>
**\<name>newproject\</name>**.

3. Make sure to run cordova emulate ios and cordova emulate android to override the old project name.

## During development

Run
```bash 
npm start
```
To test in the browser at **localhost:3000**


### To see changes on the device emulators

1. Make sure the node server is running from "npm start".

2. In /server/cordova/www/config.json change the "version" number to anything else, and include any other files that changed in the "files" array.

3. Run either of these
```bash
cordova emulate ios
cordova emulate android
```

## In Staging or Production:

In /server/cordova/www/myUpdater.html change <br>
**Updater = new H5AppFS(fileSystem, {}, 'http://localhost:3000/appUpdate/');** <br>
to the new domain and keep the /appUpdate/ appended.
