
var app = {
    deviceType: null,
    osType: null,
    
    // Application Constructor
    initialize: function() {
    },
    
    onDeviceReady: function() {
        app.deviceType = (navigator.userAgent.match(/iPad/i))  == "iPad" ? "iPad" : (navigator.userAgent.match(/iPhone/i))  == "iPhone" ? "iPhone" : (navigator.userAgent.match(/iPod/i))  == "iPod" ? "iPod" : (navigator.userAgent.match(/Android/i)) == "Android" ? "Android" : null;
        if(app.deviceType == "iPad" || app.deviceType == "iPod" || app.deviceType == "iPhone"){
            app.osType = "ios";
        } else if(app.deviceType == "Android"){
            app.osType = "android";
        }

    },
};

app.initialize();

document.addEventListener('deviceready', app.onDeviceReady, false);
