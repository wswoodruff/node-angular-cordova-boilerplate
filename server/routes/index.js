var express = require("express"),
	app = express(),
	router = express.Router(),
	cordovaDir = require("path").join(__dirname, "../cordova/www");

// GET
router.get('/', function(req, res, next) {
	res.sendFile(cordovaDir);
});
router.get('/ping', require("./ping"));

module.exports = router;
